package iakbandung.imovieapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import iakbandung.imovieapp.R;
import iakbandung.imovieapp.adapter.RecyclerAdapter;
import iakbandung.imovieapp.api.ApiClient;
import iakbandung.imovieapp.api.ApiInterface;
import iakbandung.imovieapp.entity.MovieResponse;
import iakbandung.imovieapp.entity.Result;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopRatedFragment extends Fragment {

    RecyclerView recyclerView;
    public TopRatedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final String TAG = "NowPlayingFragment";
        final View rootView = inflater.inflate(R.layout.fragment_top_rated,container, false);
        rootView.setTag(TAG);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(new LinearLayoutManager(
                getActivity().getBaseContext()));


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<MovieResponse> call = apiService.list("top_rated",ApiClient.API_KEY);
        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                List<Result> movie = response.body().getResults();
                RecyclerAdapter adapter = new RecyclerAdapter(getActivity(), movie);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                AppCompatActivity activity = (AppCompatActivity) getContext();
                Fragment myFragment = new KoneksiErrorFragment();

                activity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_movies_list, myFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        return rootView;
    }

}
