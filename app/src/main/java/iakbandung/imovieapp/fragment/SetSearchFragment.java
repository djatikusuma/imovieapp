package iakbandung.imovieapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import iakbandung.imovieapp.R;
import iakbandung.imovieapp.adapter.RecyclerAdapter;
import iakbandung.imovieapp.entity.Result;

/**
 * A simple {@link Fragment} subclass.
 */
public class SetSearchFragment extends Fragment {

    private  List<Result> movie;
    RecyclerView recyclerView;
    public SetSearchFragment(List<Result> movie) {
        // Required empty public constructor
        this.movie = movie;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final String TAG = "NowPlayingFragment";
        final View rootView = inflater.inflate(R.layout.fragment_now_playing,container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(new LinearLayoutManager(
                getActivity().getBaseContext()));

        RecyclerAdapter adapter = new RecyclerAdapter(getActivity(), movie);
        recyclerView.setAdapter(adapter);

        return rootView;
    }

}
