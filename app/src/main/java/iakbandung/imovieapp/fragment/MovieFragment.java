package iakbandung.imovieapp.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import iakbandung.imovieapp.R;
import iakbandung.imovieapp.adapter.MovieTabFragmentPagerAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends Fragment {
    private ViewPager pager;
    private TabLayout tabs;

    public MovieFragment() {
        // Required empty public constructor
    }

    /*public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_movie,container, false);
        super.onCreate(savedInstanceState);
        /*Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.tool_bar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);*/

        //((MainActivity)getActivity()).getSupportActionBar().setTitle(R.string.app_name);

        //inisialisasi tab dan pager
        pager = (ViewPager) rootView.findViewById(R.id.pager);
        tabs = (TabLayout) rootView.findViewById(R.id.tabs);

        //set object adapter kedalam ViewPager
        pager.setAdapter(new MovieTabFragmentPagerAdapter(getChildFragmentManager()));

        //Manimpilasi sedikit untuk set TextColor pada Tab
        tabs.setTabTextColors(getResources().getColor(R.color.clouds),
                getResources().getColor(android.R.color.white));

        //set tab ke ViewPager
        tabs.setupWithViewPager(pager);

        //konfigurasi Gravity Fill untuk Tab berada di posisi yang proposional
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);


        return rootView;
    }

}
