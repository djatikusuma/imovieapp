package iakbandung.imovieapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import iakbandung.imovieapp.MainActivity;
import iakbandung.imovieapp.R;
import iakbandung.imovieapp.adapter.TVAdapter;
import iakbandung.imovieapp.api.ApiClient;
import iakbandung.imovieapp.api.ApiInterface;
import iakbandung.imovieapp.entity.tvshow.Result;
import iakbandung.imovieapp.entity.tvshow.TVResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PopularTVFragment extends Fragment {

    RecyclerView recyclerView;

    public PopularTVFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final String TAG = "OnTheAir";
        final View rootView = inflater.inflate(R.layout.fragment_on_the_air,container, false);
        rootView.setTag(TAG);
        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(new LinearLayoutManager(
                getActivity().getBaseContext()));


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TVResponse> call = apiService.tvlist("popular",ApiClient.API_KEY);
        call.enqueue(new Callback<TVResponse>() {
            @Override
            public void onResponse(Call<TVResponse> call, Response<TVResponse> response) {
                List<Result> listtv = response.body().getResults();
                TVAdapter adapter = new TVAdapter(getActivity(), listtv);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<TVResponse> call, Throwable t) {
                AppCompatActivity activity = (AppCompatActivity) getContext();
                Fragment myFragment = new KoneksiErrorFragment();

                activity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_movies_list, myFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

        return rootView;
    }

}
