package iakbandung.imovieapp.fragment;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import java.util.List;

import iakbandung.imovieapp.R;
import iakbandung.imovieapp.api.ApiClient;
import iakbandung.imovieapp.api.ApiInterface;
import iakbandung.imovieapp.entity.MovieResponse;
import iakbandung.imovieapp.entity.Result;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    Button btnSearch;
    EditText txtSearch;
    Fragment fragment;
    ProgressBar progressBar;
    FrameLayout itemdetail;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_search,container, false);

        btnSearch = (Button) rootView.findViewById(R.id.btn_search);
        txtSearch = (EditText) rootView.findViewById(R.id.txtSearch);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        itemdetail = (FrameLayout) rootView.findViewById(R.id.contentsearch);
        hideLoading();
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String query = txtSearch.getText().toString();
                if(query != null || query != ""){
                    showLoading();
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    Call<MovieResponse> call = apiService.searchlist(query,ApiClient.API_KEY);
                    call.enqueue(new Callback<MovieResponse>() {
                        @Override
                        public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                            if(response.body()!=null) {
                                List<Result> movie = response.body().getResults();
                                if (movie.size() < 1) {
                                    Snackbar.make(rootView, "Pencarian tidak ditemukan", Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();

                                } else {
                                    Fragment fragment;
                                    fragment = new SetSearchFragment(movie);

                                    // Insert the fragment by replacing any existing fragment
                                    FragmentManager fragmentManager = getChildFragmentManager();
                                    fragmentManager.beginTransaction().replace(R.id.contentsearch, fragment).commit();
                                }
                                hideLoading();
                            }else{
                                Snackbar.make(rootView, "Form tidak boleh kosong", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                                hideLoading();
                            }
                        }

                        @Override
                        public void onFailure(Call<MovieResponse> call, Throwable t) {
                            AppCompatActivity activity = (AppCompatActivity) getContext();
                            Fragment myFragment = new KoneksiErrorFragment();

                            activity.getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.frame_movies_list, myFragment)
                                    .addToBackStack(null)
                                    .commit();

                            hideLoading();
                        }
                    });
                }else{
                    Snackbar.make(rootView, "Form tidak boleh kosong", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }



            }
        });

        return rootView;

    }
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        itemdetail.setVisibility(View.GONE);
    }


    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
        itemdetail.setVisibility(View.VISIBLE);
    }

}
