package iakbandung.imovieapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import iakbandung.imovieapp.adapter.VideoAdapter;
import iakbandung.imovieapp.api.ApiClient;
import iakbandung.imovieapp.api.ApiInterface;
import iakbandung.imovieapp.entity.tvshow.video.Result;
import iakbandung.imovieapp.entity.tvshow.video.VideoResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailTVActivity extends AppCompatActivity {

    TextView title,deskripsi,tahun,rated;
    ImageView txtImage;
    CollapsingToolbarLayout collapsingToolbarLayout;
    RecyclerView recycler_view;
    Context context;
    ProgressBar progressBar;
    RelativeLayout itemdetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title = (TextView) findViewById(R.id.txtJudul);
        deskripsi = (TextView) findViewById(R.id.txtDeskripsi);
        tahun = (TextView) findViewById(R.id.txtTahun);
        txtImage = (ImageView) findViewById(R.id.image);
        rated = (TextView) findViewById(R.id.rated);
        recycler_view = (RecyclerView) findViewById(R.id.listtrailer);

        LinearLayoutManager layoutManager = new LinearLayoutManager(
                getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recycler_view);
        recycler_view.setLayoutManager(layoutManager);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        itemdetail = (RelativeLayout) findViewById(R.id.itemDetail);

        showLoading();

        final Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        String dt = formateDateFromstring("yyyy-MM-dd", "dd, MMM yyyy", bundle.getString("RILIS"));

        Picasso.with(getApplicationContext())
                .load(ApiClient.IMAGE_ENDPOINT+bundle.getString("IMAGE"))
                .placeholder(R.drawable.indonesiaandroidkejar).into(txtImage);

        title.setText(bundle.getString("JUDUL"));
        deskripsi.setText(bundle.getString("DESKRIPSI"));
        tahun.setText(dt);
        rated.setText(bundle.getString("RATED"));
        long id = bundle.getLong("ID");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<VideoResponse> trailer = apiService.video(id,ApiClient.API_KEY);
        trailer.enqueue(new Callback<VideoResponse>() {
            @Override
            public void onResponse(Call<VideoResponse> trailer, Response<VideoResponse> response) {
                List<Result> trailers = response.body().getResults();
                VideoAdapter adapters = new VideoAdapter(DetailTVActivity.this, trailers);
                recycler_view.setAdapter(adapters);
                hideLoading();
            }

            @Override
            public void onFailure(Call<VideoResponse> trailer, Throwable t) {

            }
        });

        //load data trailer

        collapsingToolbarLayout.setTitle("Detail Video");
    }

    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        itemdetail.setVisibility(View.GONE);
    }


    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
        itemdetail.setVisibility(View.VISIBLE);
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputDate;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}