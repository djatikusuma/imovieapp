
package iakbandung.imovieapp.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Trailer {

    @SerializedName("id")
    private Long mId;
    @SerializedName("results")
    private List<TrailerResult> mResults;

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public List<TrailerResult> getResults() {
        return mResults;
    }

    public void setResults(List<TrailerResult> results) {
        mResults = results;
    }

}
