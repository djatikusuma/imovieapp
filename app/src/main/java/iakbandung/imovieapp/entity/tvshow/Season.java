
package iakbandung.imovieapp.entity.tvshow;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Season {

    @SerializedName("air_date")
    private String mAirDate;
    @SerializedName("episode_count")
    private Long mEpisodeCount;
    @SerializedName("id")
    private Long mId;
    @SerializedName("poster_path")
    private String mPosterPath;
    @SerializedName("season_number")
    private Long mSeasonNumber;

    public String getAirDate() {
        return mAirDate;
    }

    public void setAirDate(String airDate) {
        mAirDate = airDate;
    }

    public Long getEpisodeCount() {
        return mEpisodeCount;
    }

    public void setEpisodeCount(Long episodeCount) {
        mEpisodeCount = episodeCount;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String posterPath) {
        mPosterPath = posterPath;
    }

    public Long getSeasonNumber() {
        return mSeasonNumber;
    }

    public void setSeasonNumber(Long seasonNumber) {
        mSeasonNumber = seasonNumber;
    }

}
