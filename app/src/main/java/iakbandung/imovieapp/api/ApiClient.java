package iakbandung.imovieapp.api;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lukman on 15/08/2017.
 */

public class ApiClient {

    public static final String ENDPOINT = "http://api.themoviedb.org/3/";
    public static final String IMAGE_ENDPOINT = "http://image.tmdb.org/t/p//w500";
    public static final String API_KEY = "7bb288bb1cd4e320e390252b6592710b";
    public static final String YOUTUBE_ENDPOINT = "https://www.youtube.com/watch?v=";
    public static final String YOUTUBE_IMAGE = "https://img.youtube.com/vi/";
    public static Retrofit retrofit = null;

    public static Retrofit getClient(){
        if (retrofit == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            OkHttpClient okHttpClient = builder.build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }

}
