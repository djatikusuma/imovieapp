package iakbandung.imovieapp.api;

import iakbandung.imovieapp.entity.Movie;
import iakbandung.imovieapp.entity.MovieResponse;
import iakbandung.imovieapp.entity.Trailer;
import iakbandung.imovieapp.entity.tvshow.DetailResponse;
import iakbandung.imovieapp.entity.tvshow.TVResponse;
import iakbandung.imovieapp.entity.tvshow.video.VideoResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by lukman on 15/08/2017.
 */

public interface ApiInterface {
    @GET("movie/{sortBy}")
    Call<MovieResponse> list(
            @Path("sortBy") String sortBy,
            @Query("api_key") String apikey);

    @GET("search/movie")
    Call<MovieResponse> searchlist(
            @Query("query") String query,
            @Query("api_key") String apikey);

    @GET("tv/{sortBy}")
    Call<TVResponse> tvlist(
            @Path("sortBy") String sortBy,
            @Query("api_key") String apikey);

    @GET("movie/{movieId}")
    Call<Movie> movie(
            @Path("movieId") Long movieId,
            @Query("api_key") String apikey);

    @GET("tv/{tvId}")
    Call<DetailResponse> tvdetail(
            @Path("tvId") Long tvId,
            @Query("api_key") String apikey);

    @GET("movie/{movieId}/videos")
    Call<Trailer> trailer(
            @Path("movieId") long movieId,
            @Query("api_key") String apikey);

    @GET("tv/{tvId}/videos")
    Call<VideoResponse> video(
            @Path("tvId") long tvId,
            @Query("api_key") String apikey);

    /*@GET("movie/{movieId}/reviews")
    Observable<ReviewResponse> review(
            @Path("movieId") int movieId,
            @Query("api_key") String apikey);*/
}

