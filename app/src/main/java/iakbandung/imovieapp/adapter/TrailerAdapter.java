package iakbandung.imovieapp.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import iakbandung.imovieapp.R;
import iakbandung.imovieapp.api.ApiClient;
import iakbandung.imovieapp.entity.TrailerResult;

/**
 * Created by lukman on 18/08/2017.
 */

public class TrailerAdapter extends  RecyclerView.Adapter<TrailerViewHolder> {

    //deklarasi variable context

    private final Context context;
    private final List<TrailerResult> trailer;

    // menampilkan list item dalam bentuk text dengan tipe data string dengan variable name

    LayoutInflater inflater;
    public TrailerAdapter(Context context, List<TrailerResult> trailer) {
        this.trailer = trailer;
        this.context = context;
        inflater=LayoutInflater.from(context);
    }
    @Override
    public TrailerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=inflater.inflate(R.layout.trailer_list, parent, false);

        TrailerViewHolder viewHolder=new TrailerViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final TrailerViewHolder holder, int position) {
        holder.teaser.setText(trailer.get(position).getType());
        Picasso.with(context).load(ApiClient.YOUTUBE_IMAGE+trailer.get(position).getKey()+"/default.jpg")
                .placeholder(R.drawable.indonesiaandroidkejar)
                .into(holder.image);
        holder.name.setText(trailer.get(position).getName());

        holder.linearLayout.setOnClickListener(clickListener);
        holder.linearLayout.setTag(holder);

    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //member aksi saat cardview diklik berdasarkan posisi tertentu
            TrailerViewHolder vholder = (TrailerViewHolder) v.getTag();

            int position = vholder.getAdapterPosition();

            Intent appintent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("vnd.youtube://"+trailer.get(position).getKey()));
            Intent webintent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(ApiClient.YOUTUBE_ENDPOINT+trailer.get(position).getKey()));
            try {
                context.startActivity(appintent);
            }catch (ActivityNotFoundException ex){
                context.startActivity(webintent);
            }


        }
    };



    @Override
    public int getItemCount() {
        return trailer.size();
    }
}
