package iakbandung.imovieapp.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import iakbandung.imovieapp.R;

/**
 * Created by lukman on 18/08/2017.
 */

public class TrailerViewHolder extends RecyclerView.ViewHolder {
    // ViewHolder akan mendeskripisikan item view yang ditempatkan di dalam RecyclerView.
    TextView teaser,name; //deklarasi textview
    ImageView image;  //deklarasi imageview
    CardView cardview;
    LinearLayout linearLayout;

    public TrailerViewHolder(View itemView) {
        super(itemView);

        teaser = (TextView) itemView.findViewById(R.id.teaser);
        name = (TextView) itemView.findViewById(R.id.name);
        image = (ImageView) itemView.findViewById(R.id.image);
        linearLayout = (LinearLayout) itemView.findViewById(R.id.cardView);
    }
}