package iakbandung.imovieapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import iakbandung.imovieapp.DetailActivity;
import iakbandung.imovieapp.R;
import iakbandung.imovieapp.api.ApiClient;
import iakbandung.imovieapp.api.ApiInterface;
import iakbandung.imovieapp.entity.Genre;
import iakbandung.imovieapp.entity.Movie;
import iakbandung.imovieapp.entity.Result;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lukman on 15/08/2017.
 */

public class RecyclerAdapter extends  RecyclerView.Adapter<RecyclerViewHolder> {

    //deklarasi variable context

    private final Context context;
    private final List<Result> movie;
    List<Genre> genre;

    // menampilkan list item dalam bentuk text dengan tipe data string dengan variable name

    LayoutInflater inflater;
    public RecyclerAdapter(Context context, List<Result> movie) {
        this.movie = movie;
        this.context = context;
        inflater=LayoutInflater.from(context);
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=inflater.inflate(R.layout.movie_list, parent, false);

        RecyclerViewHolder viewHolder=new RecyclerViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        holder.title.setText(movie.get(position).getOriginalTitle());
        Picasso.with(context).load(ApiClient.IMAGE_ENDPOINT+movie.get(position).getPosterPath())
                .placeholder(R.drawable.indonesiaandroidkejar)
                .into(holder.image);
        SimpleDateFormat df = new SimpleDateFormat("yyyy");
        try {
            Date date = df.parse(movie.get(position).getReleaseDate());
            String year = df.format(date);
            holder.rilis.setText(year);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.rated.setText(movie.get(position).getVoteAverage().toString()+"/10");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Movie> call = apiService.movie(movie.get(position).getId(),ApiClient.API_KEY);
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {

                if(response.body() != null) {
                    genre = response.body().getGenres();
                    String genreString = "";
                    for (int position = 0; position < genre.size(); position++) {
                        String pre = position == (genre.size() - 1) ? "" : ", ";
                        genreString += genre.get(position).getName() + pre;
                    }
                    holder.genre.setText(genreString);
                }else{
                    holder.genre.setText("N/A");
                }

            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {

            }
        });

        holder.cardview.setOnClickListener(clickListener);
        holder.cardview.setTag(holder);

    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //member aksi saat cardview diklik berdasarkan posisi tertentu
            RecyclerViewHolder vholder = (RecyclerViewHolder) v.getTag();

            int position = vholder.getAdapterPosition();

            Intent intent = new Intent(context, DetailActivity.class);
            intent.putExtra("JUDUL", movie.get(position).getOriginalTitle());
            intent.putExtra("RILIS",movie.get(position).getReleaseDate());
            intent.putExtra("RATED", movie.get(position).getVoteAverage()+"/10");
            intent.putExtra("DESKRIPSI", movie.get(position).getOverview());
            intent.putExtra("IMAGE", movie.get(position).getBackdropPath());
            intent.putExtra("ID", movie.get(position).getId());
            context.startActivity(intent);

            /*Bundle data = new Bundle();
            data.putString("JUDUL", movie.get(position).getOriginalTitle());
            data.putString("RILIS",movie.get(position).getReleaseDate());
            data.putString("RATED", movie.get(position).getVoteAverage()+"/10");
            data.putString("DESKRIPSI", movie.get(position).getOverview());
            data.putString("IMAGE", movie.get(position).getBackdropPath());

            AppCompatActivity activity = (AppCompatActivity) v.getContext();
            Fragment myFragment = new DetailMovieFragment();

            myFragment.setArguments(data);

            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.flContent, myFragment)
                    .addToBackStack(null)
                    .commit();*/

        }
    };



    @Override
    public int getItemCount() {
        return movie.size();
    }
}