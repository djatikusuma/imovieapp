package iakbandung.imovieapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import iakbandung.imovieapp.fragment.NowPlayingFragment;
import iakbandung.imovieapp.fragment.PopularMoviesFragment;
import iakbandung.imovieapp.fragment.TopRatedFragment;
import iakbandung.imovieapp.fragment.UpcomingMoviesFragment;

/**
 * Created by lukman on 15/08/2017.
 */

public class MovieTabFragmentPagerAdapter extends FragmentPagerAdapter {
    //nama tab nya
    String[] title = new String[]{
            "Now Playing", "Upcoming", "Popular", "Top Rated"
    };

    public MovieTabFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    //method ini yang akan memanipulasi penampilan Fragment dilayar
    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position){
            case 0:
                fragment = new NowPlayingFragment();
                break;
            case 1:
                fragment = new UpcomingMoviesFragment();
                break;
            case 2:
                fragment = new PopularMoviesFragment();
                break;
            case 3:
                fragment = new TopRatedFragment();
                break;
            default:
                fragment = new NowPlayingFragment();
                break;
        }

        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public int getCount() {
        return title.length;
    }
}