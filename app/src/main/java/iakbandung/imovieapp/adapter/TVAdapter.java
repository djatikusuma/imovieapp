package iakbandung.imovieapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import iakbandung.imovieapp.DetailTVActivity;
import iakbandung.imovieapp.R;
import iakbandung.imovieapp.api.ApiClient;
import iakbandung.imovieapp.api.ApiInterface;
import iakbandung.imovieapp.entity.tvshow.DetailResponse;
import iakbandung.imovieapp.entity.tvshow.Genre;
import iakbandung.imovieapp.entity.tvshow.Result;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lukman on 18/08/2017.
 */

public class TVAdapter extends  RecyclerView.Adapter<RecyclerViewHolder> {

    //deklarasi variable context

    private final Context context;
    private final List<Result> listtv;
    List<Genre> genre;

    // menampilkan list item dalam bentuk text dengan tipe data string dengan variable name

    LayoutInflater inflater;
    public TVAdapter(Context context, List<Result> listtv) {
        this.listtv = listtv;
        this.context = context;
        inflater=LayoutInflater.from(context);
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=inflater.inflate(R.layout.movie_list, parent, false);

        RecyclerViewHolder viewHolder=new RecyclerViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        holder.title.setText(listtv.get(position).getOriginalName());
        Picasso.with(context).load(ApiClient.IMAGE_ENDPOINT+listtv.get(position).getPosterPath())
                .placeholder(R.drawable.indonesiaandroidkejar)
                .into(holder.image);
        SimpleDateFormat df = new SimpleDateFormat("yyyy");
        try {
            Date date = df.parse(listtv.get(position).getFirstAirDate());
            String year = df.format(date);
            holder.rilis.setText(year);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.rated.setText(listtv.get(position).getVoteAverage().toString()+"/10");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<DetailResponse> call = apiService.tvdetail(listtv.get(position).getId(),ApiClient.API_KEY);
        call.enqueue(new Callback<DetailResponse>() {
            @Override
            public void onResponse(Call<DetailResponse> call, Response<DetailResponse> response) {
                if(response.body() != null) {
                    genre = response.body().getGenres();
                    String genreString = "";
                    for (int position = 0; position < genre.size(); position++) {
                        String pre = position == (genre.size() - 1) ? "" : ", ";
                        genreString += genre.get(position).getName() + pre;
                    }
                    holder.genre.setText(genreString);
                }else{
                    holder.genre.setText("N/A");
                }
            }

            @Override
            public void onFailure(Call<DetailResponse> call, Throwable t) {

            }
        });

        holder.cardview.setOnClickListener(clickListener);
        holder.cardview.setTag(holder);

    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //member aksi saat cardview diklik berdasarkan posisi tertentu
            RecyclerViewHolder vholder = (RecyclerViewHolder) v.getTag();

            int position = vholder.getAdapterPosition();

            Intent intent = new Intent(context, DetailTVActivity.class);
            intent.putExtra("JUDUL", listtv.get(position).getOriginalName());
            intent.putExtra("RILIS",listtv.get(position).getFirstAirDate());
            intent.putExtra("RATED", listtv.get(position).getVoteAverage()+"/10");
            intent.putExtra("DESKRIPSI", listtv.get(position).getOverview());
            intent.putExtra("IMAGE", listtv.get(position).getBackdropPath());
            intent.putExtra("ID", listtv.get(position).getId());
            context.startActivity(intent);

            /*Bundle data = new Bundle();
            data.putString("JUDUL", movie.get(position).getOriginalTitle());
            data.putString("RILIS",movie.get(position).getReleaseDate());
            data.putString("RATED", movie.get(position).getVoteAverage()+"/10");
            data.putString("DESKRIPSI", movie.get(position).getOverview());
            data.putString("IMAGE", movie.get(position).getBackdropPath());

            AppCompatActivity activity = (AppCompatActivity) v.getContext();
            Fragment myFragment = new DetailMovieFragment();

            myFragment.setArguments(data);

            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.flContent, myFragment)
                    .addToBackStack(null)
                    .commit();*/

        }
    };



    @Override
    public int getItemCount() {
        return listtv.size();
    }
}