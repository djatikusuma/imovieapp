package iakbandung.imovieapp.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import iakbandung.imovieapp.R;

/**
 * Created by lukman on 15/08/2017.
 */

public class RecyclerViewHolder extends RecyclerView.ViewHolder {
    // ViewHolder akan mendeskripisikan item view yang ditempatkan di dalam RecyclerView.
    TextView title,rilis,rated,genre; //deklarasi textview
    ImageView image;  //deklarasi imageview
    CardView cardview;

    public RecyclerViewHolder(View itemView) {
        super(itemView);

        title = (TextView) itemView.findViewById(R.id.title);
        rilis = (TextView) itemView.findViewById(R.id.rilis);
        rated = (TextView) itemView.findViewById(R.id.rated);
        genre = (TextView) itemView.findViewById(R.id.genre);
        image = (ImageView) itemView.findViewById(R.id.image);
        cardview = (CardView) itemView.findViewById(R.id.cardView);
    }
}
