package iakbandung.imovieapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import iakbandung.imovieapp.fragment.AiringTodayTVFragment;
import iakbandung.imovieapp.fragment.OnTheAirFragment;
import iakbandung.imovieapp.fragment.PopularTVFragment;
import iakbandung.imovieapp.fragment.TopRatedTVFragment;

/**
 * Created by lukman on 15/08/2017.
 */

public class TVTabFragmentPagerAdapter extends FragmentPagerAdapter {
    //nama tab nya
    String[] title = new String[]{
            "Airing Today", "On The Air", "Popular", "Top Rated"
    };

    public TVTabFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    //method ini yang akan memanipulasi penampilan Fragment dilayar
    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position){
            case 0:
                fragment = new AiringTodayTVFragment();
                break;
            case 1:
                fragment = new OnTheAirFragment();
                break;
            case 2:
                fragment = new PopularTVFragment();
                break;
            case 3:
                fragment = new TopRatedTVFragment();
                break;
            default:
                fragment = new AiringTodayTVFragment();
                break;
        }

        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public int getCount() {
        return title.length;
    }
}